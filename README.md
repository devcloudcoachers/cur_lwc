# LWC Course Examples

This is a SFDX project that contains examples for the LWC course. 

1. Create a scratch org from a dev hub which has LWC enabled

	sfdx force:auth:web:login -d
	sfdx force:org:create -d 30 -f config/project-scratch-def.json -s

2. To deploy the exercises, 
	
	2.1. Execute 

	sfdx force:source:push

	2.2. Assign yourself the "LWC Course" permission set sfdx force:user:permset:assign -n LWC_Course

3. To deploy the final project:

	3.1. Install Dreamhouse app code from https://github.com/dreamhouseapp/dreamhouse-sfdx#installing-dreamhouse-using-salesforce-dx

	3.2. Remove **final-project-app from .forceignore file

	3.3. Push the code executing:

		sfdx force:source:push


