import { LightningElement, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import getPropertyListLwc from '@salesforce/apex/PropertyController.getPropertyListLwc';

export default class PropertyTileListLwc extends LightningElement {
	searchKey = '';
	numberBedrooms = '0';
	numberBathrooms = '0';

	@wire(CurrentPageReference) pageRef;

	@wire(getPropertyListLwc, {
		searchKey: '$searchKey',
		numberBedrooms: '$numberBedrooms',
		numberBathrooms: '$numberBathrooms'
	})
	properties;

	connectedCallback() {
		registerListener('filterchange', this.handleFilterChange, this);
	}

	disconnectedCallback() {
		unregisterAllListeners(this);
	}

	handleFilterChange(payload) {
		this.searchKey = payload.searchKey;
		this.numberBedrooms = payload.numberBedrooms;
		this.numberBathrooms = payload.numberBathrooms;
	}
}