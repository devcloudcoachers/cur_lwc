import { LightningElement, track } from 'lwc';

export default class Exercise8b extends LightningElement {
	@track
	helloWorld = 'hello world!';

	handleClick() {
		this.helloWorld = 'goodbye world!';
	}
}
