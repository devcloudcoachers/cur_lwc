import { LightningElement, api } from 'lwc';

export default class Exercise21 extends LightningElement {

    @api classBold = false;
    @api classColor = '#fff';
    @api classItalic = false;
    @api buttonName = 'Submit';

    handleClick(){
        const myEvent = new CustomEvent('propertiesselected',
                                        {
                                            detail: {
                                                bold: this.classbold,
                                                color: this.classColor,
                                                italic: this.classItalic
                                            }
                                        });
        this.dispatchEvent(myEvent);
    }
}