import { LightningElement, wire, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { NavigationMixin } from 'lightning/navigation';

export default class Exercise32b extends NavigationMixin(LightningElement) {

	@track showPanel;

	@wire(CurrentPageReference)
	currentPageReference;

	connectedCallback() {
		this.showPanel = this.currentPageReference.state.c__showPanel;
	}

	handleClick() {
		this.showPanel = !this.showPanel;
		const showPanelInUrl = this.showPanel ? true : undefined;
		const newPageReference = Object.assign({}, this.currentPageReference,
			{
				state: Object.assign({}, this.currentPageReference.state,
										{
											c__showPanel : showPanelInUrl
										})
			});

		this[NavigationMixin.Navigate](newPageReference);
	}
}