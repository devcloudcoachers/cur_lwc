import tmpl1 from './exercise20b-1.html';
import tmpl2 from './exercise20b-2.html';
import { LightningElement, api } from 'lwc';

export default class Exercise20b extends LightningElement {
	@api showTemplate1;

	render () {
		if (this.showTemplate1)
			return tmpl1;
		return tmpl2;
	}
}