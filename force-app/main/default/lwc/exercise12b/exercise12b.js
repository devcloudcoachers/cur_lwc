import { LightningElement } from 'lwc';
import { callApex } from './exercise12bExtension.js';

export default class Exercise12b extends LightningElement {
	constructor() {
		super();
		callApex();
	}
}