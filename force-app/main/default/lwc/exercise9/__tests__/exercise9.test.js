import { createElement } from 'lwc';
import Exercise9 from 'c/exercise9';

describe('c/exercise9', () => {

	afterEach(() => {
		while (document.body.firstChild) {
			document.body.removeChild(document.body.firstChild);
		}
	});

	it('displays expected public property value', () => {
		// GIVEN
		const itemName = 'item 1';

		// WHEN
		const cmpElement = createElement('c-exercise9', { is: Exercise9 });
		cmpElement.itemName = itemName;
		document.body.appendChild(cmpElement);

		// THEN
		const pElement = cmpElement.shadowRoot.querySelector('p');
		expect(pElement.textContent).toBe(itemName);
	});
});