import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class Exercise33 extends LightningElement {

	handleClick() {
		const evt = new ShowToastEvent({
			title: 'Hide yourself!',
			message: 'A huge error occurred',
			variant: 'error'
		});
		this.dispatchEvent(evt);
	}
}