import { LightningElement, wire, track } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import ACCOUNTSOURCE_FIELD from '@salesforce/schema/Account.AccountSource';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import LEADSOURCE_FIELD from '@salesforce/schema/Contact.LeadSource';

export default class Exercise26 extends LightningElement {

	@track fieldPicklistValues = {};
	objectName = ACCOUNT_OBJECT;
	fieldName = ACCOUNTSOURCE_FIELD;

	@wire(getObjectInfo, { objectApiName: '$objectName'})
	objectInfo;

	get objectInfoSerialized() {
		return JSON.stringify(this.objectInfo);
	}

	@wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: '$fieldName' })
	setPicklistValues({ error, data }) {
		this.fieldPicklistValues.data = JSON.stringify(data);
		this.fieldPicklistValues.error = error;
	}

	handleClick() {
		if (this.objectName === ACCOUNT_OBJECT) {
			this.objectName = CONTACT_OBJECT;
			this.fieldName = LEADSOURCE_FIELD;
		} else {
			this.objectName = ACCOUNT_OBJECT;
			this.fieldName = ACCOUNTSOURCE_FIELD;
		}
	}
}