import { LightningElement, track } from 'lwc';

export default class Exercise16 extends LightningElement {

	@track value1;
	@track value2;

	handleChange(event) {
		this.value2 = event.target.value;
	}

	handleClick() {
		alert('value1=' + this.value1 + '; value2=' + this.value2);
	}
}