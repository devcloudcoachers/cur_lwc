import { LightningElement } from 'lwc';
import exercise7b from '@salesforce/resourceUrl/exercise7b';
import { loadStyle } from 'lightning/platformResourceLoader';

export default class Exercise7b extends LightningElement {
	renderedCallback() {
		loadStyle(this, exercise7b)
		.then(() => {
			alert('Library has been loaded');
		})
		.catch(error => {
			alert('An error occurred');
		});
	}
}