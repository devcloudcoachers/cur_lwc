import { LightningElement, track } from 'lwc';

export default class Exercise17Wrapper extends LightningElement {
	@track orangesEaten;

	handleClick() {
		this.orangesEaten = this.template.querySelector('c-exercise17').getOrangesEaten(5);
	}
}
