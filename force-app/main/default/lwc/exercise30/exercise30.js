import { LightningElement, track } from 'lwc';
import getAccounts from '@salesforce/apex/Exercise28Controller.getAccounts';

export default class Exercise30 extends LightningElement {
	@track accounts = {};

	constructor() {
		super();
		getAccounts({ accountSource: 'Web' })
			.then(result => {
				this.accounts.data = result;
			})
			.catch(error => {
				this.accounts.error = error;
			});
	}
}