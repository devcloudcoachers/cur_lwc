import { LightningElement, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

export default class Exercise33b1 extends LightningElement {

	@wire(CurrentPageReference) pageRef;

	handleClick() {
		fireEvent(this.pageRef, 'propertiesselected', {
			bold: true,
			italics: false,
			color: '#fff'
		});
	}
}