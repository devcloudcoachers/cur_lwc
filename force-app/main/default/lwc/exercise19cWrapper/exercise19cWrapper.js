import { LightningElement, track } from 'lwc';

export default class Exercise19cWrapper extends LightningElement {
	@track showChild = true;

	handleClick(){
		this.showChild = !this.showChild;
	}
}