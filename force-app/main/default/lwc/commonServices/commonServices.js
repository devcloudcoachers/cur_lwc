const callApex = () => {
	alert('Call Apex 1');
};

const callApex2 = function() {
	alert('Call Apex 2');
};

function callApex3() {
	alert('Call Apex 3');
}

export {
	callApex,
	callApex2,
	callApex3
};