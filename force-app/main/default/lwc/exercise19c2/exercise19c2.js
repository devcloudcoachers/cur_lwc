import { LightningElement } from 'lwc';

export default class Exercise19c2 extends LightningElement {
		connectedCallback() {
			console.log('grandchild connected callback');
		}
	
		disconnectedCallback() {
			console.log('grandchild disconnected callback');
		}
	}