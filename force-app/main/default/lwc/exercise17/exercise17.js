import { LightningElement, api } from 'lwc';

export default class Exercise17 extends LightningElement {

	@api
	getOrangesEaten(orangeNumber) {
		return `I have eaten ${orangeNumber} oranges`;
	}
}