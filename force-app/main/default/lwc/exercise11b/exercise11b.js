import { LightningElement, track, api } from 'lwc';

export default class Exercise11b extends LightningElement {
	@track itemNameUpperCase;
	itemNameInternal;

	@api 
	get itemName() {
		return this.itemNameInternal;
	}

	set itemName(value) {
		this.itemNameInternal = value;
		this.itemNameUpperCase = value.toUpperCase();
	}
}