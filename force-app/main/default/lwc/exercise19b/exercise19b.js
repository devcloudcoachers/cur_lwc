import { LightningElement } from 'lwc';

export default class Exercise19b extends LightningElement {

	connectedCallback() {
		console.log('Child connected callback');
	}

	renderedCallback() {
		console.log('Child rendered callback');
	}
}