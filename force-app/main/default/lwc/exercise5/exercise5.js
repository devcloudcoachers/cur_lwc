import { LightningElement, track } from 'lwc';

export default class Exercise5 extends LightningElement {
	@track showError = true;

	handleClick() {
		this.showError = !this.showError;
	}
}
