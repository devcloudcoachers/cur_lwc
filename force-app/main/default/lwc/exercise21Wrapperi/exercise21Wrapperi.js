import { LightningElement, track } from 'lwc';

export default class Exercise21Wrapper extends LightningElement {
	@track clickedButtonName;
	@track details;

	renderedCallback(){
		const childs = this.template.querySelectorAll('c-exercise21i');

		childs.forEach((child) => { 
			child.addEventListener('propertiesselected',this.listenProperty.bind(this));
		});
	}

	listenProperty(event){
		this.details = JSON.stringify(event.detail);
		this.clickedButtonName = event.target.buttonName;
	}
}