import { LightningElement, track } from 'lwc';

export default class Exercise20bWrapper extends LightningElement {
	@track showTemplate1;

	handleClick() {
		this.showTemplate1 = !this.showTemplate1;
	}
}