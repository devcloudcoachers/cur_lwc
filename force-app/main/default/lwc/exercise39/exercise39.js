import { LightningElement, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import NAME_FIELD from '@salesforce/schema/Account.Name';

export default class Exercise39 extends LightningElement {
	@wire(getRecord, { recordId: '0011x00000JyTKLAA3', fields: [NAME_FIELD]})
	account;
}
