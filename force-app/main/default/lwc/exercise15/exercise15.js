import { LightningElement, track } from 'lwc';

export default class Exercise15 extends LightningElement {

	@track emojis;

	constructor() {
		super();
		const url = 'https://api.github.com/emojis';

		/* OPTION 1
		
		fetch(url)
			.then(resp => resp.text()) // function(resp) { return resp.text(); }
			.then(text => { this.emojis = text; })// function(data) { this.emojis = data; }
			.catch(error => {
				console.log('Error:' + error);
			});*/

		/* OPTION 2 */
		const me = this;
		fetch(url)
			.then(function(resp){
					return resp.text();
				}) 
			.then(function(text){ 
					me.emojis = text;
				})
			.catch(error => {
				console.log('Error:' + error);
			});
	}
}