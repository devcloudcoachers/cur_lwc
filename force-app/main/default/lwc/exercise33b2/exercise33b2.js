import { LightningElement, wire, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';

export default class Exercise33b2 extends LightningElement {

	@track properties;
	@wire(CurrentPageReference) pageRef;

	connectedCallback() {
		registerListener('propertiesselected', this.handlePropertiesSelected, this);
	}

	disconnectedCallback() {
		unregisterAllListeners(this);
	}

	handlePropertiesSelected(properties) {
		this.properties = JSON.stringify(properties);
	}
}