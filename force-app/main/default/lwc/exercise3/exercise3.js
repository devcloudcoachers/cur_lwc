import { LightningElement } from 'lwc';

export default class Exercise3 extends LightningElement {
	helloWorld = 'hello world!';

	handleClick(event) {
		event.target.setAttribute('hidden', 'true');
	}
}
