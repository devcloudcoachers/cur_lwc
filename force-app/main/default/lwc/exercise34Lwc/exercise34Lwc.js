import { LightningElement, api } from 'lwc';

export default class Exercise34Lwc extends LightningElement {
	@api message1;

	@api showMessage(message2) {
		alert(this.message1 + message2);
	}
}