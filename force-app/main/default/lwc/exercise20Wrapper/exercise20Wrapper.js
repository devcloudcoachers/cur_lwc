import { LightningElement, track } from 'lwc';

export default class Exercise20Wrapper extends LightningElement {
	@track error;
	@track stack;

	errorCallback(error, stack) {
		this.error = error;
		this.stack = stack;
	}
}