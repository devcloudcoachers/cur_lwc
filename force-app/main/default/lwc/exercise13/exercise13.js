import { LightningElement } from 'lwc';
import { callApex, callApex2 as myCall2, callApex3 } from 'c/commonServices';

export default class Exercise13 extends LightningElement {
	constructor() {
		super();
		callApex();
		myCall2();
		callApex3();
	}
}