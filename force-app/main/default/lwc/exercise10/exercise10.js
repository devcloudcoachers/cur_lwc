import { LightningElement, api } from 'lwc';

export default class Exercise10 extends LightningElement {
	@api itemName = 'Default item';

	handleClick() {
		this.itemName = 'Other name';
	}
}