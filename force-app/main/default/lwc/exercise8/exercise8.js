import { LightningElement } from 'lwc';

export default class Exercise8 extends LightningElement {
	helloWorld = 'hello world!';

	handleClick() {
		this.helloWorld = 'goodbye world!';
	}
}
