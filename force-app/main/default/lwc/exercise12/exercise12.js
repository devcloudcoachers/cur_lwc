import { LightningElement, api } from 'lwc';

export default class Exercise12 extends LightningElement {
	privateItemName = 'Default item';

	renderedCallback() {
		this.template.querySelector('div').className = 'newClass';
	}

	@api
	get itemName() {
		return this.privateItemName;
	}

	set itemName(value) {
		this.privateItemName = value;
		this.setAttribute('item-name', this.privateItemName);
	}
}