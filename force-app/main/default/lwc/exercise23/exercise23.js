import { LightningElement, api } from 'lwc';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import ACCOUNT_NUMBER_FIELD from '@salesforce/schema/Account.AccountNumber';
import ACCOUNT_DESCRIPTION_FIELD from '@salesforce/schema/Account.Description';

export default class Exercise23 extends LightningElement {
	@api recordId;

	accountObject = ACCOUNT_OBJECT;
	accountFields = [ACCOUNT_NUMBER_FIELD,ACCOUNT_DESCRIPTION_FIELD];
}