// openFileSample.js
import { LightningElement, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getFiles from '@salesforce/apex/exercise32cClass.getFiles';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class exercise32c extends NavigationMixin(LightningElement) {
   filesIds;

    @wire(getFiles)
    function({data,error}){
        if (data) {
          this.filesIds = data;
        }else if (error) {
          this.error = error;
        }
    }
    navigateToFiles() {
      if(this.filesIds != ''){
          this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state : {
                recordIds: this.filesIds
            }
          })
      }else{
          const evt = new ShowToastEvent({
            title: 'Error',
            message: 'Debe introducir imágenes en Salesforce Files',
            variant: 'error'
          });
          this.dispatchEvent(evt);
      }
    }

}