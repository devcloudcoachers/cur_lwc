import { LightningElement } from 'lwc';
import myCustomLabel from '@salesforce/label/c.myCustomLabel';
import lang from '@salesforce/i18n/lang';

export default class Exercise19 extends LightningElement {
	currentUserLanguage = lang;
	myCustomLabel = myCustomLabel;
}