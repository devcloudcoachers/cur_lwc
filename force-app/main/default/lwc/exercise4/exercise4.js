import { LightningElement } from 'lwc';

export default class Exercise4 extends LightningElement {
	helloWorld = 'hello world!';

	get helloWorldModified() {
		return this.helloWorld + ' Nice to see you!';
	}
}
