import { LightningElement, wire } from 'lwc';
import getAccounts from '@salesforce/apex/Exercise28Controller.getAccounts';

export default class Exercise28 extends LightningElement {
	@wire(getAccounts, { accountSource: 'Web' })
	accounts;
}