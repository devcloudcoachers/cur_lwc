import { LightningElement } from 'lwc';

export default class Exercise19c1 extends LightningElement {
	connectedCallback() {
		console.log('child connected callback');
	}

	disconnectedCallback() {
		console.log('child disconnected callback');
	}
}