import { LightningElement } from 'lwc';

export default class Exercise19bWrapper extends LightningElement {
	connectedCallback() {
		console.log('Parent connected callback');
	}

	renderedCallback() {
		console.log('Parent rendered callback');
	}
}