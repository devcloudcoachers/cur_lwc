({
	handlePropertiesSelected : function(cmp, event, helper) {
		const bold = event.getParam('bold');
		const italics = event.getParam('italics');
		const color = event.getParam('color');
		alert(`Selected properties: bold=${bold}, italics=${italics}, color=${color}`);
	}
})