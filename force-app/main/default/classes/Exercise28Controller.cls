public with sharing class Exercise28Controller {
	@AuraEnabled(cacheable=true)
	public static List<Account> getAccounts(String accountSource) {
		// Try error case with:
		// throw new AuraHandledException('ex');
		return [SELECT Name FROM Account WHERE AccountSource = :accountSource];
	}
}