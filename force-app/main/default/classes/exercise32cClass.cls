public with sharing class exercise32cClass {
    @AuraEnabled(cacheable=true)
    public static String getFiles() {
		String idsDoc = '';
		for(ContentDocument docs :[SELECT Id FROM ContentDocument LIMIT 2]){
            idsDoc += docs.Id + ',';
        }
        if(idsDoc != ''){
            idsDoc = idsDoc.removeEnd(',');
        }
        
        return idsDoc;
	}
}
